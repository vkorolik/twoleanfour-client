package net.minecraft.src;

import java.awt.*;
import net.minecraft.client.Minecraft;
import net.minecraft.client.MinecraftApplet;
import org.lwjgl.LWJGLException;

public class MinecraftAppletImpl extends Minecraft
{
    /** Reference to the main frame, in this case, the applet window itself. */
    final MinecraftApplet mainFrame;

    public MinecraftAppletImpl(MinecraftApplet par1MinecraftApplet, Component par2Component, Canvas par3Canvas, MinecraftApplet par4MinecraftApplet, int par5, int par6, boolean par7)
    {
        super(par2Component, par3Canvas, par4MinecraftApplet, par5, par6, par7);
        mainFrame = par1MinecraftApplet;
        // -----------------------------------------------------------------------------------------------------------------------
        ZMod.initialize(this);
        // -----------------------------------------------------------------------------------------------------------------------
    }

    /**
     * Displays an unexpected error that has come up during the game.
     */
    public void displayUnexpectedThrowable(UnexpectedThrowable par1UnexpectedThrowable)
    {
        mainFrame.removeAll();
        mainFrame.setLayout(new BorderLayout());
        mainFrame.add(new PanelCrashReport(par1UnexpectedThrowable), "Center");
        mainFrame.validate();
    }

    // ---------------------------------------------------------------------------------------------------------------------------
    public void startGame() throws LWJGLException {
        super.startGame();
        ZMod.initOverrides();
    }

    public void runTick() {
        ZMod.pingUpdateHandle();
        super.runTick();
    }

    public void respawn(boolean flag, int kst, boolean flag2) {
        super.respawn(flag, kst, flag2);
        ZMod.pingRespawnHandle(flag);
    }
    // ---------------------------------------------------------------------------------------------------------------------------

}
